document.addEventListener('DOMContentLoaded', () => {
  const form = document.querySelector('form[name="signup"]');
  const emailInput = form.querySelector('input[type="email"]');
  const submitBtn = form.querySelector('button[type="submit"]');
  emailInput.addEventListener('input', (_) => {
    if (form.checkValidity()) {
      submitBtn.classList.remove('cursor-not-allowed');
      submitBtn.classList.add('cursor-pointer');
      form.removeAttribute('disabled');
    } else {
      form.setAttribute('disabled', 'disabled');
      submitBtn.classList.remove('cursor-pointer');
      submitBtn.classList.add('cursor-not-allowed');
    }
  });
  form.addEventListener('submit', (e) => e.preventDefault());
});
