/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {
      fontFamily: {
        'josefin-sans': ['Josefin Sans', 'sans-serif']
      },
      backgroundImage: {
        'pattern-desktop': 'url("./images/bg-pattern-desktop.svg")',
        'custom-gradient-1': 'linear-gradient(135deg, hsl(0, 80%, 86%), hsl(0, 74%, 74%))',
        'custom-gradient-2': 'linear-gradient(135deg, hsl(0, 0%, 100%), hsl(0, 100%, 98%))'
      },
      colors: {
        'desaturated-red': 'hsl(0, 36%, 70%)',
        'soft-red': 'hsl(0, 93%, 68%)',
        'dark-greyish-red': 'hsl(0, 6%, 24%)'
      }
    },
  },
  plugins: [],
}
